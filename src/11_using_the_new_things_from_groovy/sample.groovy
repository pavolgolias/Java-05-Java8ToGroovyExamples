boolean isGT3(int number) {
  println "isGT3 $number"
  number > 3
}
  
boolean isEven(int number) {
  println "isEven $number"
  number % 2 == 0
}
  
int doubleIt(int number) {
  println "doubleIt $number"
  number * 2
}

def numbers = [1, 2, 3, 5, 4, 6, 7, 8, 9, 10]

//traditional Groovy functional style is eager.
def eagerResult = numbers.findAll { isGT3(it) }.findAll { isEven(it) }.collect { doubleIt(it) }.find { it }
println "Eager Result: $eagerResult"
//We can use Java 8 Streams and be lazy - meaning efficient

def result = numbers.stream()
                    .filter { isGT3(it) }
                    .filter { isEven(it) }
                    .map { doubleIt(it) }
                    .findFirst()
                    .get()
    
println "Result: $result"
    
println "Let's see the effect of laziness"

numbers.stream()
       .filter { isGT3(it) }
       .filter { isEven(it) }
       .map { doubleIt(it) }
                  
println "No work expended"