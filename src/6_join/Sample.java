import java.util.*;
import static java.util.stream.Collectors.joining;

public class Sample {
  public static void main(String[] args) {
    List<String> names = Arrays.asList("Jack", "Sara", "Jill");
    
    System.out.println(
      names.stream()
           .map(String::toUpperCase)
           .collect(joining(", ")));
  }
}
