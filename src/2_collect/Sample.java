import java.util.*;
import java.util.stream.Collectors;

public class Sample {
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);

    numbers.stream()
           .map(e -> e * 2)
           .forEach(System.out::println);

    System.out.println(
      numbers.stream()
             .map(e -> e * 2)
             .collect(Collectors.toList()));
  }
}




