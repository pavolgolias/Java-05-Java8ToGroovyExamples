import java.util.*;

public class Sample {
  public static boolean isGT3(int number) {
    System.out.println("isGT3 " + number);
    return number > 3;
  }
  
  public static boolean isEven(int number) {
    System.out.println("isEven " + number);
    return number % 2 == 0;
  }
  
  public static int doubleIt(int number) {
    System.out.println("doubleIt " + number);
    return number * 2;
  }

  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 5, 4, 6, 7, 8, 9, 10);

    int result = numbers.stream()
                        .filter(Sample::isGT3)
                        .filter(Sample::isEven)
                        .map(Sample::doubleIt)
                        .findFirst()
                        .get();
    
    System.out.println("Result: " + result);
    
    System.out.println("Let's see the effect of laziness");
    numbers.stream()
           .filter(Sample::isGT3)
           .filter(Sample::isEven)
           .map(Sample::doubleIt);
           
    System.out.println("No work expended");
  }
}
