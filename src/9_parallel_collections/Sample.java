import java.util.*;
import java.util.stream.Stream;

public class Sample {
  public static int doubleIt(int number) {
    try { Thread.sleep(1000); } catch(Exception ex) {} //assume a computation intensive code here
    return number * 2;
  }
  
  public static int doubleAndTotal(Stream<Integer> values) {
    long start = System.nanoTime();
    try {
      return values.mapToInt(Sample::doubleIt)
                   .sum();      
    } finally {
      long end = System.nanoTime();
      System.out.println("Time taken (s): " + (end - start) / 1.0e9);
    }
  }
  
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    
    System.out.println(doubleAndTotal(numbers.stream()));

    System.out.println(doubleAndTotal(numbers.parallelStream()));
  }
}
