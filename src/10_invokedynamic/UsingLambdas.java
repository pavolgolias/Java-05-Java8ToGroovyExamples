import java.util.*;

public class UsingLambdas {
  public static void main(String[] args) {
    List<Integer> values = Arrays.asList(1, 2, 3);

    values.forEach(e -> System.out.println(e));
    values.forEach(e -> System.out.println(e));
    values.forEach(e -> System.out.println(e));
    values.forEach(e -> System.out.println(e));
    values.forEach(e -> System.out.println(e));
  }
}

//javac UsingLambdas
//ls (or dir)
//See the lack of inner classes.
//Do javap -c UsingLambdas
//examine the bytecode and look for invokedynamic
//now remove the .class files before procedding to the other example
