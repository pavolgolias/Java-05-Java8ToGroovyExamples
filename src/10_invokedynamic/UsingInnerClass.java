import java.util.*;
import java.util.function.Consumer;

public class UsingInnerClass {
  public static void main(String[] args) {
    List<Integer> values = Arrays.asList(1, 2, 3);

    values.forEach(new Consumer<Integer>() {
     public void accept(Integer value) {
       System.out.println(value);
     }  
    });

    values.forEach(new Consumer<Integer>() {
     public void accept(Integer value) {
       System.out.println(value);
     }  
    });

    values.forEach(new Consumer<Integer>() {
     public void accept(Integer value) {
       System.out.println(value);
     }  
    });

    values.forEach(new Consumer<Integer>() {
     public void accept(Integer value) {
       System.out.println(value);
     }  
    });

    values.forEach(new Consumer<Integer>() {
     public void accept(Integer value) {
       System.out.println(value);
     }  
    });
  }
}

//javac UsingInnerClass
//ls (or dir)
//See the inner classes.
//now remove the .class files before procedding to the other example
