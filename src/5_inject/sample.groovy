def numbers = [1, 2, 3, 4, 5, 6]

println numbers.collect { it * 2 }.inject(0) { c, e -> c + e }
