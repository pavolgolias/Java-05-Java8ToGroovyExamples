import java.util.*;

public class Sample {
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);

    System.out.println(
      numbers.stream()
             .map(e -> e * 2)
             .reduce(0, (c, e) -> c + e));

    System.out.println(
      numbers.stream()
             .map(e -> e * 2)
             .reduce(0, Math::addExact));

  }
}
