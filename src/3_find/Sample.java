import java.util.*;

public class Sample {
  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6);

    System.out.println(
      numbers.stream()
             .filter(e -> e % 2 == 0)
             .findFirst());

    System.out.println(
      numbers.stream()
             .filter(e -> e % 2 == 0)
             .findFirst()
             .get());

    System.out.println(
      numbers.stream()
             .filter(e -> e % 2 == 0)
             .findFirst()
             .orElse(0));    
  }
}
