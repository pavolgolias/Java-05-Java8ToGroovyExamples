import java.util.*;
import java.util.stream.Stream;
import static java.util.stream.Collectors.toList;

public class Sample {
  public static List<Integer> seriesOfDouble(int size) {
    return Stream.iterate(1, e -> e * 2)
                 .limit(size)
                 .collect(toList());
  }
  
  public static void main(String[] args) {
    System.out.println(seriesOfDouble(5));

    System.out.println(seriesOfDouble(10));
  }
}
