## Java 8 for Groovy Programmers

**These examples were download from http://agiledeveloper.com/downloads.html**


Java 8 - what's in it for us?

From the (Groovy) familiar to:
- iteration
- collect
- find
- findAll
- inject
- join

The unfamiliar
- default methods
- static methods in interface as well
- method references
- lazy evaluations
- Infinite series
- parallel collections
- invokeDynamic
- Using the new things from Groovy

Author: Venkat Subramaniam
